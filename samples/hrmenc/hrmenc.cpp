//
// hrmenc.cpp
//
// Product: HRM SDK
//
// Implements HRM exercise file encode console app.
//
// Copyright Copyright (C) 2014 Markku Pulkkinen. All rights reserved.
//
// SPDX-License-Identifier: BSL-1.0
//
#include "stdafx.h"
#include "hrmsdk.h"

// map for [I/O format type string, HRMSDK Format enum]
static const std::map<const std::string, hrmsdk::Format> formatMap =
{
    {"hrm", hrmsdk::HRM},
    {"ppt", hrmsdk::PPT},
    {"gpx", hrmsdk::GPX},
    {"tcx", hrmsdk::TCX},
    {"fit", hrmsdk::FIT},
    {"pdf", hrmsdk::PDF}
};

// help string
static const std::string HELP_FOOTER(
    "examples:\n" \
    "\n   - hrmenc /i hrm /o tcx /s 43\n" \
    "      converts and saves hrm exercise files to tcx format:\n"
    "         exercise time range: current year, week 43\n" \
    "\n   - hrmenc /i hrm /o gpx /s 2014.8.10\n" \
    "      converts and saves hrm exercise files to gpx format:\n" \
    "         exercise time range: 10 August 2014\n" \
    "\n   - hrmenc /i hrm /o ppt /s 2013.2 user:pwd\n" \
    "      converts and uploads hrm exercise files to Polar Personal Trainer:\n" \
    "         exercise time range: February, 2013\n" \
    "         user, pwd: Polar Personal Trainer login user name, password\n" \
    "         (Note: PPT upload must be enabled in configuration file settings)");

// usage string
static const std::string HELP_USAGE(
    "hrmenc [/i input-format] /o output-format /s scope [username:password]\n");

/// @class HrmEncoderApplication
class HrmEncoderApplication : public Poco::Util::Application
{
public:
    /// constructor
    HrmEncoderApplication():
        helpRequested_(false),
        err_("")
    {}
protected:
    /// main entry point for app
    int main(const std::vector<std::string> &args);
    /// defines command-line options for app
    void defineOptions(Poco::Util::OptionSet &options);
private:
    /// true if help requested
    bool helpRequested_;
    /// error description
    std::string err_;
    /// local date/time
    Poco::LocalDateTime now_;
    /// conversion request parameters
    hrmsdk::ConversionRequest request_;

    /// help option handler
    void handleHelp(
        const std::string &name,
        const std::string &value);
    /// input format option handler
    void handleInputFormat(
        const std::string &name,
        const std::string &value);
    /// output format option handler
    void handleOutputFormat(
        const std::string &name,
        const std::string &value);
    /// scope option handler
    void handlePeriod(
        const std::string &name,
        const std::string &value);
    /// displays help text
    void displayHelp();
    /// set scope period/week
    void setPeriodYearOrWeek(int val);
    /// set scope month
    void setPeriodMonth(int val);
    /// set scope day
    void setPeriodDay(int val);
    /// set exercise id
    void setPeriodExerciseId(int val);
};

POCO_APP_MAIN(HrmEncoderApplication)

///////////////////////////////////////////////////////////////////////////////
int HrmEncoderApplication::main(const std::vector<std::string> &args)
{
    if (helpRequested_)
    {
        displayHelp();
        return EXIT_OK;
    }
    if (!err_.empty())
    {
        std::cerr << err_ << std::endl;
        return EXIT_USAGE;
    }
    int ret(EXIT_OK);
    using namespace hrmsdk;
    ExerciseProcessor &exerciseProcessor = ExerciseProcessor::instance(POLAR);
    do
    {
        ret = exerciseProcessor.init();
        if (ret) break;
        request_.args = args;
        ret = exerciseProcessor.convert(request_);
    }
    while (0);
    std::cout << std::endl << "hrmsdk::ExerciseProcessor returned: " << ret;
    exerciseProcessor.exit();
    return ret;
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::defineOptions(Poco::Util::OptionSet &options)
{
    Application::defineOptions(options);

    options.addOption(
        Poco::Util::Option("help", "h",
                           "displays help and exit\n")
        .required(false)
        .repeatable(false)
        .callback(Poco::Util::OptionCallback<HrmEncoderApplication>(this,
                  &HrmEncoderApplication::handleHelp)));
    options.addOption(
        Poco::Util::Option("?", "?",
                           "displays help and exit\n")
        .required(false)
        .repeatable(false)
        .callback(Poco::Util::OptionCallback<HrmEncoderApplication>(this,
                  &HrmEncoderApplication::handleHelp)));
    options.addOption(
        Poco::Util::Option("input-format", "i",
                           "defines input file format\n")
        .required(false)
        .repeatable(false)
        .argument("hrm|ppt [default: hrm]")
        .callback(Poco::Util::OptionCallback<HrmEncoderApplication>(this,
                  &HrmEncoderApplication::handleInputFormat)));
    options.addOption(
        Poco::Util::Option("output-format", "o",
                           "defines output format\n")
        .required(true)
        .repeatable(false)
        .argument("gpx|tcx|fit|ppt")
        .callback(Poco::Util::OptionCallback<HrmEncoderApplication>(this,
                  &HrmEncoderApplication::handleOutputFormat)));
    options.addOption(
        Poco::Util::Option("scope", "s",
                           "defines time range for exercise(s)")
        .required(true)
        .repeatable(false)
        .argument("week number|year[.month][.day][.exercise id]")
        .callback(Poco::Util::OptionCallback<HrmEncoderApplication>(this,
                  &HrmEncoderApplication::handlePeriod)));
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::handleHelp(
    const std::string &name,
    const std::string &value)
{
    helpRequested_ = true;
    stopOptionsProcessing();
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::displayHelp()
{
    Poco::Util::HelpFormatter helpFormatter(options());
    helpFormatter.setIndent(3);
    helpFormatter.setUsage(HELP_USAGE);
    helpFormatter.setHeader("options:");
    helpFormatter.setFooter(HELP_FOOTER);
    helpFormatter.format(std::cout);
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::handleInputFormat(
    const std::string &name,
    const std::string &value)
{
    try
    {
        std::string str(Poco::trim(value));
        Poco::toLowerInPlace(str);
        auto it = formatMap.find(str);
        if (it != formatMap.end())
        {
            request_.input = it->second;
            return;
        }
        err_ = "unknown input format: " + value;
    }
    catch (const std::exception &e)
    {
        err_ = e.what();
    }

    if (!err_.empty())
    {
        stopOptionsProcessing();
    }
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::handleOutputFormat(
    const std::string &name,
    const std::string &value)
{
    try
    {
        std::string str(Poco::trim(value));
        Poco::toLowerInPlace(str);
        auto it = formatMap.find(str);
        if (it != formatMap.end())
        {
            request_.output = it->second;
            return;
        }
        err_ = "unknown output format: " + value;
    }
    catch (const std::exception &e)
    {
        err_ = e.what();
    }

    if (!err_.empty())
    {
        stopOptionsProcessing();
    }
}

//////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::handlePeriod(
    const std::string &name,
    const std::string &value)
{
    try
    {
        std::string str(Poco::trim(value));
        Poco::toLowerInPlace(str);
        // extract scope values from string
        using Poco::StringTokenizer;
        int opt(StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
        StringTokenizer st(str, ".-/", opt);
        for (auto i = 0; i < st.count(); i++)
        {
            // exit loop if week is set
            if (request_.scope.week > 0)
                break;

            int val(Poco::NumberParser::parse(st[i]));
            switch (i)
            {
            default:
                break;
            case 0:
                setPeriodYearOrWeek(val);
                break;
            case 1:
                setPeriodMonth(val);
                break;
            case 2:
                setPeriodDay(val);
                break;
            case 3:
                setPeriodExerciseId(val);
                break;
            }
        }
    }
    catch (const std::exception &e)
    {
        err_ = e.what();
    }
    if (!err_.empty())
    {
        stopOptionsProcessing();
    }
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::setPeriodYearOrWeek(int val)
{
    if (val >= 1 && val <= 54)
    {
        request_.scope.year = now_.year();
        request_.scope.week = val;
    }
    else if (val >= 1980 && val <= now_.year())
    {
        request_.scope.year = val;
    }
    else
    {
        throw std::invalid_argument("invalid year or week value for scope");
    }
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::setPeriodMonth(int val)
{
    bool isCurrentYear(now_.year() == request_.scope.year);
    int maxMonth = (isCurrentYear ? now_.month() : 12);
    if (val >= 1 && val <= maxMonth)
    {
        request_.scope.month = val;
    }
    else
    {
        throw std::invalid_argument("invalid month value for scope");
    }
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::setPeriodDay(int val)
{
    bool isCurrentYear(now_.year() == request_.scope.year);
    bool isCurrentMonth(now_.month() == request_.scope.month);
    int maxDay = (isCurrentYear && isCurrentMonth ?
                  now_.day() :
                  Poco::DateTime::daysOfMonth(
                      request_.scope.year,
                      request_.scope.month));
    if (val >= 1 && val <= maxDay)
    {
        request_.scope.day = val;
    }
    else
    {
        throw std::invalid_argument("invalid day value for scope");
    }
}

///////////////////////////////////////////////////////////////////////////////
void HrmEncoderApplication::setPeriodExerciseId(int val)
{
    if (val >= 1 && val <= 99)
    {
        request_.scope.id = val;
    }
    else
    {
        throw std::invalid_argument("invalid exercise id value for scope");
    }
}