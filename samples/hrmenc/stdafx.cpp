//
// stdafx.cpp
//
// Product: HRM SDK
//
// Source file that includes just the standard includes.
// hrmenc.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information
//
// Reference any additional headers you need in stdafx.h file
// and not in this file.
//
// Copyright Copyright (C) 2014 Markku Pulkkinen. All rights reserved.
//
// SPDX-License-Identifier: BSL-1.0
//
#include "stdafx.h"
