    Heart Rate Monitor(HRM) Software Development Toolkit
    Copyright © 2014 Markku Pulkkinen 
    12/5/2014 11:55:19 AM 

**README**

Console application to demonstrate HRMSDK usage and features.

**Build**

Use Google GYP build automation tool to generate native IDE project/make files. GYP supports GCC/make (Linux, Solaris), MS VisualStudio (MS Windows), XCode (Mac OSX), Clang/make (Mac OSX, Linux) native build files. 

- hrmenc.gyp: Google GYP build file.

**Source Files**

- hrmenc.cpp: This is the main application source file.
- stdafx.[h,cpp]: These files are used for precompiling headers.
 
**External Libraries**

- hrmsdk.[h,lib]: HRMSDK API header and link library file.
- PocoFoundation.lib, PocoUtil.lib, PocoXML.lib, PocoJSON.lib : Poco Open Source C++ class libraries.

**Application Usage**

hrmencode [/i input-format] /o output-format /s scope [username:password]
 
Options

    /help
    /? 
      displays help and exit.
       
    /input-format = hrm | ppt[default: hrm]
      defines input file format.
       
    /output-format = gpx | tcx | fit | ppt
      defines output format.
       
    /scope = week number | year[.month][.day][.exercise id]
      defines time range for exercises.

Examples

    - hrmencode /i hrm /o tcx /s 43
       * converts and saves hrm exercise files to tcx format.
       * exercise time range: current year, week 43
  
    - hrmencode /i hrm /o gpx /s 2014.8.10
       * converts and saves hrm exercise files to gpx format.
       * exercise time range: 10th August 2014

    - hrmencode /i hrm /o ppt /s 2013.2 user:pwd
       * converts and uploads hrm exercise files to Polar Personal Trainer web service.
       * exercise time range: February, 2013
       * user, pwd: Polar Personal Trainer login user name, password
         (Note: PPT upload must be enabled in configuration file settings)

**References**

Poco Open Source Project: [http://pocoproject.org](http://pocoproject.org "Poco Project")

Google GYP Build Automation Tool: [http://en.wikipedia.org/wiki/GYP_%28software%29](http://en.wikipedia.org/wiki/GYP_%28software%29 "Google GYP build tool")

