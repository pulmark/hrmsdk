//
// stdafx.h
//
// Product: HRM SDK
//
// Defines header files for precompiler.
//
// Copyright Copyright (C) 2014 Markku Pulkkinen. All rights reserved.
//
// SPDX-License-Identifier: BSL-1.0
//
#ifndef STDAFX_H_
#define STDAFX_H_

#ifdef _WIN32
#include "targetver.h"
#endif

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/LocalDateTime.h"
#include "Poco/DateTimeParser.h"
#include "Poco/String.h"
#include "Poco/StringTokenizer.h"
#include "Poco/NumberParser.h"

#endif
