# GYP Build file for HRMSDK toolkit
# Copyright (C) 2014, Markku Pulkkinen. All rights reserved.
{
    'variables': {
     'conditions': [
       ['OS=="win"', {
         'poco_dir': '$(POCO_ROOT)',
       }, {
       }],
     ]
    },
    'includes': [
        'common.gypi'
    ],
    'targets': [
        {
            'target_name': 'hrmenc',
            'product_name': 'hrmencode',
            'type': 'executable',
            'defines' : ['POCO_STATIC'],
            'msvs_precompiled_header': 'samples/hrmenc/stdafx.h',
            'msvs_precompiled_source': 'samples/hrmenc/stdafx.cpp',
            'sources': [
                '<!@(ls -1 samples/hrmenc/*.cpp)',
                '<!@(ls -1 samples/hrmenc/*.h)'
            ],
            'include_dirs': [
                'include/',
                '<(poco_dir)/Foundation/include/',
                '<(poco_dir)/Util/include/',
                '<(poco_dir)/XML/include/',
                '<(poco_dir)/JSON/include/',
            ],
            'library_dirs': [
              'lib/',
              '<(poco_dir)/lib/'
            ],
            'conditions': [
                ['OS=="win"', {
                    'libraries': [
                        '-lAdvapi32.lib',
                        '-lhrmsdk.lib'
                    ],
                }],
            ],
            'direct_dependent_settings': {
                'include_dirs': [ 
                    'include/' 
                ],
            },
        },
    ]
}