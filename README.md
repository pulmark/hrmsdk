    Heart Rate Monitor(HRM) Software Development Toolkit
    Copyright © 2014 Markku Pulkkinen 
    12/5/2014 11:55:19 AM 

**README**

Toolkit for developing applications to create or to publish exercise data
in various, well-known formats. As extension for commonly recorded data, power calculation for cycling is provided.

**Exercise Data Format Conversion**

Polar HRM devices uses a specific format for recorded activities. 
This data from the device can be uploaded into Polar web service or 
saved locally to view and to analyze with Polar software.

Toolkit provides API to convert exercise data into other, more
commonly used formats. The converted data is saved into a file or 
imported into Polar Personal Trainer web service. The following I/O formats are supported:

Inputs:

- data imported from Polar device, HRM or GPX format,
- data exported from Polar Personal Trainer
 
Outputs:

- GPS Exchange Format (GPX),  
- Garmin Training Center XML (TCX),
- Flexible and InterOperable Data Transfer Protocol (FIT), 
- Polar Personal Trainer Import

**Power Calculation for Cycling**

The cycling power calculation uses recorded speed, 
altitude changes, acceleration and riding direction if present to calculate power required to push the bike.
 
To provide more accurate estimate for power, wind speed wind direction and also rider and bike specific properties like tyre rolling resistance and aero-dynamic drag can be entered and utilized in the calculation.

**Benefits** 

1. By using the applications developed with toolkit, Polar HRM device users can 
publish their exercise data on web services that support commonly used
GPX, TCX or FIT formats or use converted data for other purposes. 

2. Athletes with a long training backlog using Polar ProTrainer PC app can upload their exercise data history into [https://www.polarpersonaltrainer.com](https://www.polarpersonaltrainer.com "Polar Personal Trainer") web service directly.

3. Power calculation provides valuable information about the athletes performance. This information can be utilized to optimize training and to build training plans.

**Possible Solutions**

A PC service app that runs in the background and transparently converts exercise data into TCX or FIT format and saves data into users Dropbox ([http://www.dropbox.com](http://www.dropbox.com "Dropbox")) account when exercise data is imported from HRM device. 

By using Tapiriik ([https://tapiriik.com](https://tapiriik.com "Tapiriik")) web app synchronized with Dropbox, it is possible automatically transfer exercise data into Strava, TrainingPeaks or into other supported web sites.

This solution allows users to publish exercise data in various sites by only transferring data once from HRM device into local computer. 
          

**Download**

Official releases of HRM toolkit are pushed into a Bitbucket git repository at [https://bitbucket.org/pulmark/hrmsdk](https://bitbucket.org/pulmark/hrmsdk "hrmsdk"). 

These include HRM toolkit shared library binaries, headers and configuration files, examples with source code, installation package for evaluation and API documents.

**Questions or Comments ?**

Send email to: support@markkupulkkinen.com