//
// hrmsdk.h
//
// Product: HRM SDK
//
// Defines API for hrmsdk shared library.
//
// Copyright Copyright (c) 2014, Markku Pulkkinen. All rights reserved.
//
#ifndef HRMSDK_H_
#define HRMSDK_H_

#if defined(_WIN32) && defined(_WINDLL)
#if defined(HRMSDK_EXPORTS)
#define HRMSDK_API __declspec(dllexport)
#else
#define HRMSDK_API __declspec(dllimport)
#endif
#endif

#if !defined(HRMSDK_API)
#if defined (__GNUC__) && (__GNUC__ >= 4)
#define HRMSDK_API __attribute__ ((visibility ("default")))
#else
#define HRMSDK_API
#endif
#endif

#include "ExerciseProcessor.h"

#endif
