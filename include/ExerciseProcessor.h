//
// ExerciseProcessor.h
//
// Library: hrmsdk.lib
//
// Defines API for exercise data processing.
//
// Copyright Copyright (c) 2014, Markku Pulkkinen. All rights reserved.
//
#ifndef EXERCISE_PROCESSOR_H_
#define EXERCISE_PROCESSOR_H_

#include <string>
#include <vector>
#include <iostream>

/// HRM SDK lib namespace.
namespace hrmsdk
{

/// Identifiers for HRM device vendors.
enum VendorId
{
    POLAR = 1   ///< Polar Electro Inc.
};

/// Input/Output format of exercise data to use for convert().
enum Format
{
    UNDEF = 0,      ///< undefined
    HRM,            ///< Polar HRM and optional GPX
    TCX,            ///< Garmin Training Center Database XML (XML)
    GPX,            ///< Topografix GPX Track XML (XML)
    FIT,            ///< Ant Alliance Fitness Data Protocol (binary)
    PPT,            ///< Polar Personal Trainer Web Service
    PDF             ///< Adobe PDF (not implemented)
};

/// Returned error codes.
enum ErrorCode
{
    /// Operation succeeded.
    SUCCESS = 0,
    /// Operation not implemented.
    ERR_NOT_IMPLEMENTED = 1000,
    /// Invalid product license.
    ERR_LICENSE_INVALID,
    /// Product license is expired.
    ERR_LICENSE_EXPIRED,
    /// Invalid parameter given.
    ERR_INVALID_PARAMETER,
    /// Invalid path for input/output files.
    ERR_INVALID_PATH,
    /// Invalid input/output file.
    ERR_INVALID_FILE,
    /// Invalid configuration file.
    ERR_INVALID_CONFIGURATION,
    /// Invalid conversion format.
    ERR_INVALID_CONVERSION_TYPE,
    /// Invalid conversion scope.
    ERR_INVALID_CONVERSION_SCOPE,
    /// Not ready to process request.
    ERR_INVALID_STATE,
    /// Conversion failure.
    ERR_CONVERSION_FAILED,
    /// Conversion type not yet supported.
    ERR_CONVERSION_TYPE_NOT_SUPPORTED,
    /// No suitable files found within scope.
    ERR_CONVERSION_NO_FILES_WITHIN_SCOPE,
    /// No conversion results available.
    ERR_CONVERSION_NO_RESULTS_AVAILABLE,
    /// Authentication failure with web service.
    ERR_WEB_AUTHENTICATION_FAILED,
    /// Uploading of exercise data into web disabled.
    ERR_WEB_UPLOAD_DISABLED,
    /// Uploading of exercise data into web failed.
    ERR_WEB_UPLOAD_FAILED
};

/// @struct ConversionScope
/// Scope of exercises to convert.
/// Refer to @ref ConversionScopeUsage for more details.
struct HRMSDK_API ConversionScope
{
    /// Constructor.
    explicit ConversionScope(
        int y = 1980, int m = 0, int d = 0, int i = 0, int w = 0):
        year(y), month(m), day(d), id(i), week(w)
    {}

    int year;       ///< Year [1980,]
    int month;      ///< Month [0,12]
    int day;        ///< Day [0,31]
    int id;         ///< exercise identifier [0,99]
    int week;       ///< week number [0,54]
};

/// @struct ConversionRequest
/// Context for convert() arguments.
struct HRMSDK_API ConversionRequest
{
    /// Constructor.
    explicit ConversionRequest():
        input(Format::HRM), output(Format::TCX), result(std::cout)
    {}

    Format input;                   ///< input format
    Format output;                  ///< output format
    ConversionScope scope;          ///< scope
    std::vector<std::string> args;  ///< optional arguments
    mutable std::ostream &result;   ///< Refer to @ref ConvertResultFormat
};

/// @struct ReadRequest
/// Context for read() arguments.
struct HRMSDK_API ReadRequest
{
    mutable std::ostream &result;   ///< stream for results[JSON]
};

/// @class ExerciseProcessor
/// Interface class for exercise data processing
class HRMSDK_API ExerciseProcessor
{
public:
    /// Destructor.
    virtual ~ExerciseProcessor() {};

    /// Returns instance into exercise data processor.
    /// @param[in] vendor - vendor id for exercise data.
    static ExerciseProcessor &instance(VendorId vendor = POLAR);

    /// Initializes exercise data processor.
    virtual ErrorCode init() = 0;

    /// Exits exercise data processor.
    virtual ErrorCode exit() = 0;

    /// Reads exercise data from wearable device/bike computer etc.
    /// @param[in,out] request - read arguments
    virtual ErrorCode read(const ReadRequest &request) = 0;

    /// Runs the conversion using given arguments.
    /// @param[in,out] request - conversion arguments
    virtual ErrorCode convert(const ConversionRequest &request) = 0;
};
} // namespace hrmsdk

/** @page ConversionScopeUsage
Usage of ConversionScope for exercise selection.
HRM SDK ExerciseProcessor::convert() operation supports batch processing of
exercise files. Internally toolkit uses Input.Format.Path values defined
in configuration file to find exercise files available for conversion.
Value 0 for ConversionScope arguments is designated as asterisk(*) for
file selection.<br>
Examples:
~~~~
ConversionScope(2013) - select all exercises year 2013
ConversionScope(2013,12) - select all exercises Dec, 2013
ConversionScope(2013,12,5) - select all exercises 5th Dec, 2013
ConversionScope(2013,12,5,2) - select exercise #2 5th Dec 2013
ConversionScope(2013,0,0,0,43) - select all exercises week 43, 2013
~~~~
*/

/** @page ConvertResultFormat
Results for convert() operation are returned in following JSON format:<br>
~~~~
{
   "date": "2014-11-21T20:21:58.247959Z",
   "exerciseFormat": {
       "input": "Polar HRM",
       "output": "FIT"
   },
   "exerciseRange": {
       "begin": "2014-11-01T00:00:00Z",
       "end": "2014-11-30T00:00:00Z"
   },
   "items": [{
       "file": {
           "out.fit": "14110501.fit",
           "in.gpx": "14110501.gpx",
           "in.hrm": "14110501.hrm"
       },
       "success": true,
       "time": 0.794249
   }, {
       "file": {
           "out.fit": "14111201.fit",
           "in.gpx": "14111201.gpx",
           "in.hrm": "14111201.hrm"
       },
       "success": true,
       "time": 0.83274
   }]
}
~~~~
*/

#endif // EXERCISE_PROCESSOR_H_