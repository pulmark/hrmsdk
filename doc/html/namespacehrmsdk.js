var namespacehrmsdk =
[
    [ "ConversionRequest", "structhrmsdk_1_1_conversion_request.html", "structhrmsdk_1_1_conversion_request" ],
    [ "ConversionScope", "structhrmsdk_1_1_conversion_scope.html", "structhrmsdk_1_1_conversion_scope" ],
    [ "ExerciseProcessor", "classhrmsdk_1_1_exercise_processor.html", "classhrmsdk_1_1_exercise_processor" ],
    [ "ReadRequest", "structhrmsdk_1_1_read_request.html", "structhrmsdk_1_1_read_request" ]
];